﻿using System;
using System.Collections.Generic;
using System.Text;

namespace calculoIMC
{
    class IMC
    {
        public double CalculoIMC(Usuario usuario)
        {
           double Calculos = usuario.peso/ (usuario.Altura * usuario.Altura);

            return Calculos;
        }
                        
        public string Calculo(double imc)
        {
            if (imc < 18.5)
            {
                 return "Baixo peso";
            }

            else if (imc >= 18.5 && imc <= 24.9)
            {
                return "Peso normal";
            }
            else if (imc >= 25 && imc <= 29.9)
            {
                return "Sobrepeso";
            }
            else if (imc >= 30 && imc <= 34.9)
            {
                return "Obeso I";
            }
            else if (imc >= 35 && imc <= 39.9)
            {
                return "Obeso II";
            }
            else 
            {
                return "Obeso III";
            }

         }
    }
}
